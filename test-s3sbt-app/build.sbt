organization := "sh.serene.dummy.app"

name := "serene-hello-scala-app"

version := "1.0"

// Serene repositories for retrieving binary dependencies.
resolvers += "Serene Releases" at "s3://serene-maven-repository/releases"
resolvers += "Serene Snapshots" at "s3://serene-maven-repository/snapshots"

// A library dependency from the Serene maven repository.
libraryDependencies += "sh.serene.dummy" %% "serene-hello-scala" % "1.0-SNAPSHOT"
