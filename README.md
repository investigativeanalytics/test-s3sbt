# TEST-S3SBT

Test publishing/retrieval of artifacts  to/from Serene Maven repository, using
SBT (Scala).

This code  repository contains  two separate test  applications, that  work in
combination:

- _test-s3sbt-lib_ is a scala library offering a "hello world" function.
- _test-s3sbt-app_ is a scala application that uses the above library.

First the library is compiled and  publishing is tested.  Then the application
is built and run, testing the retrieval of the library artifact.

## Prerequisites

This test should  work Linux, OSX and Windows systems,  assuming that a recent
version of SBT is available in the command line execution path.

The test  applications are self  contained in terms of  configuration (plugins
and repository addresses), but require that the AWS credentials are configured
by the user (e.g. file `~/.aws/credentials` in Linux).

Instructions to configure the credentials can be found on Confluence.

## Testing

Starting from  the source repository root,  issue the following commands  in a
terminal. The first time the process may  require some time, as the sbt plugin
necessary to access the repository must be retrieved.

```bash
cd test-s3sbt-lib
sbt clean
sbt compile
sbt publish
```

All three sbt operations should report *success*.

```bash
cd ../cd test-s3sbt-app
sbt clean
sbt compile
sbt run
```

All three  sbt operations should  report *success* and the  application should
print the message _Hello, world!_.






