// The first 3 entries ensure correct artifact deployment (repository structure).
organization := "sh.serene.dummy"

name := "serene-hello-scala"

version := "1.0-SNAPSHOT"

// Setting for publishing Serene artifacts.
publishMavenStyle := true

publishTo := {
  if (isSnapshot.value)
    Some("Serene Snapshots" at "s3://serene-maven-repository/snapshots")
  else
    Some("Serene Releases" at "s3://serene-maven-repository/releases")
}

